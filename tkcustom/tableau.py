# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#  Auteur: xehx (Edwin HELET)
#  Date: 07/05/2020 15:05
#  Projet: Sondage (Projet Restaurant 1A)
#  Dernière modification: 07/05/2020 13:25
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#  Auteur: xehx (Edwin HELET)
#  Date: 28/04/2020 21:45
#  Projet: Sondage (Projet Restaurant 1A)
#  Dernière modification: 28/04/2020 21:45
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
from tkinter import Label
from tkinter.ttk import Button
from typing import Dict, Callable, List

from tkcustom.scrollframe import ScrolledWindow


class TableauColumnProperties:
    """
    class qui regroupe les propriétés d'une colonne pour l'object TableauFrame
    """
    def __init__(self, title: str, type=str):
        """
        :param title: titre de la colonne
        :param type: type de la colonne
        """
        self.title = title
        self.type = type

    def generate(self, master, value, **kw):
        """
        Méthode qui génère le label de la colonne pour un élément value
        :param master: le frame parent
        :param value: le dictionnaire qui comporte les données d'une ligne
        :return: le label
        """
        label = Label(master)
        if self.type == bool: label.config(text="✔" if value else "✘", bg='#fff', fg="green" if value else "red", font=('calibri', 12))
        elif self.type == [bool, int]: label.config(text="✔"+(f" ({value})" if value != float("Inf") else "") if value > 0 else f"✘", bg='#fff', fg="green" if value > 0 else "red", font=('calibri', 12))
        elif self.type == list: label.config(text=len(value), bg='#fff')
        else: label.config(text=value, bg='#fff')
        return label

    def update(self, label: 'Label', value):
        """
        Méthode qui modifie le label de la colonne pour un élément value
        :param label: le label à modifier
        :param value: le dictionnaire qui comporte les données d'une ligne
        :return:
        """
        if self.type == bool: label.config(text="✔" if value else "✘", fg="green" if value else "red")
        elif self.type == [bool, int]: label.config(text="✔"+(f" ({value})" if value != float("Inf") else "") if value > 0 else f"✘", bg='#fff', fg="green" if value > 0 else "red")
        elif self.type == list: label.config(text=len(value), bg='#fff')
        else: label.config(text=str(value), bg='#fff')

class TableauCallable(TableauColumnProperties):
    """
    class qui regroupe toute les propriétés d'un bouton clickable dans l'object TableauFrame
    """
    def __init__(self, text, command: Callable = lambda o: None, style="INFO.TButton", title=""):
        """
        :param text: le texte du bouton
        :param command: la fonction a appellé lorsque l'utilisateur clique sur ce bouton (fonction qui possède un paramètre de type 'dict')
        :param title: le titre de la colonne
        :param style: le style du bouton
        """
        super().__init__(title)
        self.text = text
        self.command = command
        self.style = style
        self.title = title

    def generate(self, master, objectCommand, **kw):
        """
        Méthode générant le button
        :param master: le frame parent
        :param objectCommand: le paramètre de type 'dict' utilisé pour appellé la fonction self.command
        :param kw: paramètre de configuration supplémentaire pour le bouton
        :return: le bouton
        """
        return Button(master, text=self.text, command=lambda: self.command(objectCommand), style=self.style, width=len(self.text)+1, **kw)

class TableauFrame(ScrolledWindow):
    """
    Créer un tableau (avec une scrollbar si nécéssaire)
    """
    def __init__(self, master, objectList: Dict[str, dict], columnView=True, padx: int = 100, bd=0, **columns: TableauColumnProperties):
        """
        :param master: le frame parent
        :param objectList:
        :param columnView: activer la visibilité des titre des colonnes ou non
        :param bd: taille de la bordure
        :param padx: padding sur l'axe x entre chaque colonne
        :param columns: une ou plusieurs colonnes de type TableauColumnProperties ou TableauCallable (pour un bouton)
        """
        super().__init__(master, width=1280, height=580, relief='solid', bg='#fff', bd=bd)
        self.padx = padx
        self.columns = columns
        self.columnView = columnView
        self.emptyLabel = Label(self.content, text="Aucun élément", bg='#fff')
        self.columnsLabelsList: List[Label] = list()
        self.labelsList = dict()

        if not objectList:
            self.emptyLabel.grid()
            return

        self.addColumns()
        for key in objectList: self.addLine(key, objectList[key])

    def addColumns(self):
        if self.columnView:
            columnIndex = 0
            for key in self.columns:
                columnLabel = Label(self.content, text=self.columns[key].title, font='Helvetica 12 bold', bg='#fff')
                columnLabel.grid(row=0, column=columnIndex, pady=10)
                self.columnsLabelsList.append(columnLabel)
                columnIndex += 1

    def removeColumns(self):
        for columnLabel in self.columnsLabelsList: columnLabel.destroy()
        self.columnsLabelsList.clear()

    def addLine(self, key, ob: dict):
        """
        Cette méthode permet d'ajouter une ligne dans la table
        :param key: la clé de l'object
        :param ob: un dictionnaire qui comporte en clé le nom des colonnes et en valeur la valeur à afficher
        :return:
        """
        lineIndex = len(self.labelsList) + 1
        labels = dict()
        columnLineIndex = 0
        for k in self.columns:
            column = self.columns[k]
            if isinstance(column, TableauCallable):
                element = column.generate(self.content, ob)
                element.grid(row=lineIndex, column=columnLineIndex, padx=(0, 20), pady=10)
            else:
                element = column.generate(self.content, ob[k])
                element.grid(row=lineIndex, column=columnLineIndex, padx=self.padx, pady=10)
            labels[k] = element
            columnLineIndex += 1

        if len(self.labelsList) == 0:
            self.emptyLabel.grid_forget()
            self.addColumns()
        self.labelsList[key] = labels

    def removeLine(self, key: str):
        """
        Retirer une ligne dans le tableau
        :param key: la clé de la ligne
        :return:
        """
        if not (key in self.labelsList): return
        for param in self.labelsList[key]:
            if param in self.columns: self.labelsList[key][param].grid_forget()

        # ne pas supprimer de la list car la ligne de placement s'effectue selon la taille de la liste
        # j'ajoute un nouvelle ligne à 5 et je supprimer 4, la prchaine sera mise à 5 et pas à 6
        # voilà pk je n'utilise pas la prochaine ligne
        # del self.labelsList[key]

        if len(self.labelsList) == 0:
            self.emptyLabel.grid()
            self.removeColumns()

    def updateValue(self, key: str, **params):
        """
        Modifier une ligne dans le tableau
        :param key: la clé de la ligne
        :param params: les nom des colonnes avec leur nouvelle valeurs
        :return:
        """
        for c in params:
            self.columns[c].update(self.labelsList[key][c], params[c])

    def updateViewColumn(self):
        self.columnView = not self.columnView
        if self.columnView: self.addColumns()
        else: self.removeColumns()


