# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#  Auteur: xehx (Edwin HELET)
#  Date: 07/05/2020 15:05
#  Projet: Sondage (Projet Restaurant 1A)
#  Dernière modification: 07/05/2020 15:05
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import traceback
from tkinter.ttk import Entry, Labelframe, Radiobutton, Button
from tkinter import Label, Toplevel, Frame, END, IntVar, Variable
from typing import Callable, Dict
import tkinter.messagebox as tm


class CustomTopLevel(Toplevel):
    """
    Cette class est un toplevel customisé
    """
    def __init__(self, master, title, resizable=True, onclose: Callable = None, **kw):
        """
        :param master: le widget parent
        :param title: le titre du TopLevel (obligatoire)
        :param resizable: la fenetre sera redimensionnable ou non
        :param onclose: function supplémentaire à effectuer à la fermeture de la fenêtre
        :param kw: paramêtres supplémentaire de configuration pour la class mère TopLevel
        """
        super().__init__(master, **kw)
        if not resizable: self.resizable(False, False)
        x = master.winfo_x()
        y = master.winfo_y()
        self.onclose = onclose
        self.title(title)
        self.geometry("+%d+%d" % (x + 100, y + 100))

    def destroy(self):
        """
        Méthode appellé lors de la destruction de la fenêtre (CustomTopLevel)
        :return:
        """
        super().destroy()
        if self.onclose is not None: self.onclose()

class EntryProperties:
    """
    Cette class est utilisé pour entrer des propriétés sur une entrée d'un formulaire
    """
    def __init__(self, title, defaultValue="", help=None, state=False):
        """
        :param title: le texte du label
        :param defaultValue: la valeur par defaut de l'entrée
        :param help: une chaine de caractère pour aider à la saisie
        """
        self.title = title
        self.defaultValue = defaultValue
        self.help = help
        self.state = state

    def create(self, master, row) -> Entry:
        """
        Cette méthode permet de générer et de placer les propriétés des l'entrée (label, entry, help)
        dans un frame qui comporte le formulaire. Je recommande d'appeller cette méthode quand tu créer ton formulaire
        :param master: le frame qui comporte le formulaire
        :param row: la ligne ou appliquer cette entrée
        :return: le widget qui comporte la valeur variable
        """
        Label(master, text=self.title+" : ", bg="#fff").grid(row=row, sticky='e', pady=10)
        entry = Entry(master, width=50)
        entry.insert(END, self.defaultValue)
        if self.help is not None:
            entry.grid(row=row, column=1, sticky='w', pady=(10, 2))
            Label(master, text=self.help, bg='#fff').grid(row=row+1, column=1, sticky='w', pady=(2, 10))
        else:
            entry.grid(row=row, column=1, sticky='w', pady=10)
        if self.state is not None: entry.configure(state=self.state)
        return entry

class RadioEntryProperties(EntryProperties):
    def __init__(self, title, values: Dict[int, str], defaultValue=0):
        super().__init__(title, defaultValue)
        self.values = values

    def create(self, master, row) -> Variable:
        """
        Cette méthode est implémenté de la méthode de la class mère et possède les même propriétées
        :param master: le frame qui comporte le formulaire
        :param row: la ligne ou appliquer cette entrée
        :return: le widget qui comporte la valeur variable
        """
        lf = Labelframe(master, text=self.title, style="RADIO.TLabelframe", width=100)
        entry = IntVar()
        entry.set(self.defaultValue)
        for e in self.values:
            Radiobutton(lf, text=self.values[e], variable=entry, value=e, style="RADIO.TRadiobutton").pack(pady=5, padx=20, anchor="w")
        lf.grid(row=row, columnspan=2, pady=10)
        return entry

class FormWindow(CustomTopLevel):
    """
    Créer un formulaire dans une fenêtre (elle implémente la class CustomTopLevel)
    """
    def __init__(self, master, function: Callable, title="Gestion du restaurant - Formulaire", message=None, onclose: Callable = None, **entries: 'EntryProperties'):
        """
        :param master: le widget (ex: frame) depuis lequel la fenêtre du formulaire sera ouverte
        :param function: fonction appellé lors de la validation du formulaire
        :param title: le titre de la fenêtre
        :param message: le message d'en-tête du formulaire
        :param onclose: fonction supplémentaire
        :param entries: les entrées à demander (ce doit être des objects de type EntryProperties)
        """
        super().__init__(master, title, resizable=False, onclose=onclose, background="#fff")
        self.grab_set()
        self.functionSubmit = function
        self.entries = dict()

        formFrame = Frame(self, bg="#fff")
        if message is not None: Label(formFrame, text=message, bg='#fff', fg="red").grid(columnspan=2, pady=10)
        line = 1
        for key in entries:
            self.entries[key] = entries[key].create(formFrame, row=line)
            line += 2

        frameSubmit = Frame(formFrame, bg='#fff')
        Button(frameSubmit, text="Appliquer", command=self.apply, style="ADD.TButton").grid(row=0, column=0, padx=20)
        Button(frameSubmit, text="Annuler", command=self.destroy, style="DEL.TButton").grid(row=0, column=1, padx=20)
        frameSubmit.grid(columnspan=2)
        formFrame.pack(padx=50, pady=20)

    def apply(self):
        """
        Méthode appellé lorsque le formulaire est envoyé
        :return:
        """
        try:
            params = dict()
            for p in self.entries:
                params[p] = self.entries[p].get()
            self.functionSubmit(**params)
            self.destroy()
        except Exception as err:
            # print(traceback.format_exc())
            tm.showerror("Erreur", err, parent=self.master)
