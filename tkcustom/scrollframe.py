# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#  Auteur: xehx (Edwin HELET)
#  Date: 07/05/2020 15:04
#  Projet: Sondage (Projet Restaurant 1A)
#  Dernière modification: 07/05/2020 15:04
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
from tkinter import Frame, Scrollbar, Canvas, TclError


class AutoScrollbar(Scrollbar):
    # a scrollbar that hides itself if it's not needed.  only
    # works if you use the grid geometry manager.
    def set(self, lo, hi):
        if float(lo) <= 0.0 and float(hi) >= 1.0:
            # grid_remove is currently missing from Tkinter!
            self.pack_forget()
        else:
            if self.cget("orient") == "horizontal":
                self.pack(side='bottom', fill='x', expand=0)
            else:
                self.pack(side="right", fill='y', expand=0)
        Scrollbar.set(self, lo, hi)

    def grid(self, **kw):
        raise TclError("cannot use grid with this widget")

    def place(self, **kw):
        raise TclError("cannot use place with this widget")

class ScrolledWindow(Frame):
    def __init__(self, parent, *args, **kw):
        """Parent = master of scrolled window
        canv_w - width of canvas
        canv_h - height of canvas

       """
        super().__init__(parent, *args, **kw)

        self.parent = parent
        frameTop = Frame(self)

        # self.xscrlbr = AutoScrollbar(self, orient='horizontal')
        self.yscrlbr = AutoScrollbar(frameTop, orient='vertical')
        # creating a canvas
        self.canv = Canvas(frameTop)
        self.canv.config(width=self.cget("width"), height=self.cget("height"), bd=0, highlightthickness=0, relief='ridge')
        # placing a canvas into frame
        self.canv.pack(side="left", fill='both', expand=1)
        self.yscrlbr.pack(side="right", fill='y', expand=0)
        frameTop.pack()
        # self.xscrlbr.pack(side='bottom', fill='x', expand=0)

        # accociating scrollbar comands to canvas scroling
        # self.xscrlbr.config(command=self.canv.xview)
        self.yscrlbr.config(command=self.canv.yview)

        # creating a frame to inserto to canvas
        self.content = content = Frame(self.canv)
        if 'bg' in kw:
            content.config(bg=kw['bg'])
            self.canv.config(bg=kw['bg'])
        elif 'backgroundcolor' in kw:
            content.config(bg=kw['backgroundcolor'])
            self.canv.config(bg=kw['backgroundcolor'])

        self.canv.create_window(0, 0, window=content, anchor='nw')

        # self.canv.config(xscrollcommand=self.xscrlbr.set, yscrollcommand=self.yscrlbr.set, scrollregion=(0, 0, 100, 100))
        self.canv.config(yscrollcommand=self.yscrlbr.set, scrollregion=(0, 0, 100, 100))

        content.bind('<Configure>', self._configure_window)
        content.bind('<Enter>', lambda event: self.canv.bind_all("<MouseWheel>", self._on_mousewheel))
        content.bind('<Leave>', lambda event: self.canv.unbind_all("<MouseWheel>"))
        return

    def _on_mousewheel(self, event):
        if self.content.winfo_reqheight() > self.canv.winfo_height():
            self.canv.yview_scroll(int(-1 * (event.delta / 120)), "units")

    def _configure_window(self, event):
        # update the scrollbars to match the size of the inner frame
        size = (self.content.winfo_reqwidth(), self.content.winfo_reqheight())
        self.canv.config(scrollregion='0 0 %s %s' % size)

        if self.content.winfo_reqwidth() != self.canv.winfo_width(): self.canv.config(width=self.content.winfo_reqwidth())
        if self.content.winfo_reqheight() != self.canv.winfo_height(): self.canv.config(height=self.content.winfo_reqheight())