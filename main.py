# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#  Auteur: xehx (Edwin HELET)
#  Date: 22/04/2020 21:42
#  Projet: Sondage (Projet Restaurant 1A)
#  Dernière modification: 22/04/2020 21:40
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
from tkinter import *
from tkinter import ttk
import tkinter.messagebox as tm

from tkcustom.formulaire import FormWindow
from tkcustom import tableau as tko
from tkcustom import formulaire as tkf
from utils import Menu, Action
import menu.gestion_menu.gestion_menu as gmenu
import menu.gestion_stock as gstock
import menu.gestion_commandes as gcommande
import menu.history_commands as history


class Restaurant(Menu):

    def __init__(self):
        self.currentCommand = None
        self.stock = gstock.GestionStock(self)
        self.menu = gmenu.GestionMenu(self, self.stock)
        self.commande = gcommande.GestionCommandes(self.menu)
        self.history_commands = history.CommandsHistory(self, self.commande)
        super().__init__([
            Action("Prise de commande", lambda: gcommande.PriseCommande(self, input("Nom du client: "), self.menu, self.commande), None, stateAction="c"),
            Action("Gestion des stocks", lambda: self.stock.openConsole(), self.stock),
            Action("Gestion du menu", lambda: self.menu.openConsole(), self.menu),
            Action("Ingrédients et disponibilité des plats", lambda: (print("Fonctionnalité non disponible pour le moment"), self.openConsole()), None),
            Action("Historique des commandes", lambda: self.history_commands.openConsole(), self.history_commands)
        ], None)
        self.window = Tk()
        tko.windowTk = self.window

    def openWindow(self):
        self.window.title("Gestion du restaurant")
        self.window.geometry("1280x720")
        self.window.minsize("1280", "720")
        self.window.config(background="#c6c6c6")
        self.window.iconbitmap(default='icon.ico')
        self.loadStyle()

        self.tab = ttk.Notebook(self.window)  # Création du système d'onglets
        self.tab.pack(fill="both", expand="yes")

        for action in self.actions:
            if action.childObject is not None:
                frameOnglet = Frame(self.tab, bg="#fff")
                frameOnglet.pack_propagate(0)
                frameOnglet.pack(fill='both', expand=1)
                self.tab.add(frameOnglet, text=action.title)
                action.addFrameToChild(frameOnglet)

        self.footer = Frame(self.window, height=40, width=self.window.winfo_width(), bg='#fff')
        self.footer.pack_propagate(0)
        ttk.Button(self.footer, text='Prise de commande', command=lambda: self.openPriseCommande(), style="INFO.TButton").grid(row=0, column=0, padx=20, pady=5, sticky='w')
        self.footer.pack(fill='x')

        self.window.protocol("WM_DELETE_WINDOW", self.close)
        self.window.mainloop()

    def loadStyle(self):
        style = ttk.Style()
        style.configure("TNotebook", background="#c6c6c6", tabmargins=[100, 0, 100, 0])
        style.configure("TNotebook.Tab", foreground='#000', padding=[20, 5])
        # style.configure('TButton', font=('calibri', 10, 'bold', 'underline'), foreground='red')
        # style.map("TNotebook.Tab", background=[("selected", "#3b3b3b")])
        style.configure('ADD.TButton', font=('calibri', 10), foreground='green')
        style.configure('ADD.TButton.Label', highlightthickness=0)
        style.configure('DEL.TButton', font=('calibri', 10), foreground='red')
        style.configure('UPD.TButton', font=('calibri', 10), foreground='orange')
        style.configure('REFRESH.TButton', font=('calibri', 10), foreground='black')
        style.configure('INFO.TButton', font=('calibri', 10), foreground='blue')

        style.configure('RADIO.TLabelframe', background='#fff')
        style.configure('RADIO.TLabelframe.Label', background='#fff')
        style.configure('RADIO.TRadiobutton', background='#fff')
        # style.map('TButton', background=[('active', 'black')])
        style.layout("Tab", [
            ('Notebook.tab', {'sticky': 'nswe', 'children':
                 [('Notebook.padding', {'side': 'top', 'sticky': 'nswe', 'children': [
                     ('Notebook.label', {'side': 'top', 'sticky': ''}),
                     # ('Notebook.focus', {'side': 'top', 'sticky': '', 'children':
                     #    [('Notebook.label', {'underline': 'True'})],
                     # })
                 ]})],
            })
        ])

    def openPriseCommande(self):
        if self.currentCommand is None:
            FormWindow(self.window, function=self.priseCommande, clientName=tkf.EntryProperties("Client"), title=f"Nouvelle commande")
        else: tm.showerror("Erreur", "Une commande est déjà en cours !")

    def priseCommande(self, clientName: str):
        if self.currentCommand is None:
            self.currentCommand = gcommande.PriseCommande(self, clientName, self.menu, self.commande)
        else: raise Exception("Une commande est déjà en cours !")

    def close(self):
        r = tm.askyesnocancel("Quitter", "Voulez-vous enregistrer les données ?")
        if r is None: return
        elif r:
            self.stock.saveProducts()
            self.menu.saveMenu()
            self.commande.saveCommandes()
        self.window.destroy()


resto = Restaurant()
resto.openWindow()
