# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#  Auteur: xehx (Edwin HELET)
#  Date: 23/04/2020 11:23
#  Projet: Sondage (Projet Restaurant 1A)
#  Dernière modification: 23/04/2020 11:23
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import datetime
import os
import json
import uuid
from tkinter import Frame, BaseWidget
from tkinter.ttk import Button
from typing import List, Callable, Dict


class Menu:

    def __init__(self, actions: List['Action'], parent: 'Menu', onupdateFrame: Callable = None):
        self.actions = actions
        self.parent = parent
        self.contentFrame: 'Frame' = None
        self.onupdateFrame = onupdateFrame

    def setFrame(self, frame: 'BaseWidget'):
        self._frame = frame
        frameAction = Frame(frame, bg="#fff")
        for action in self.actions:
            if action.isWindowAction():
                btn = Button(frameAction, text=action.title, command=action.function, style="INFO.TButton")
                if action.stateAction.lower() == "w":
                    btn.config(style="REFRESH.TButton", width=len(action.title)+1)
                btn.grid(row=0, column=self.actions.index(action), padx=40, pady=10)
        frameAction.pack()

        self.updateFrame()
        self.contentFrame.pack()

    def updateFrame(self):
        if self.contentFrame is not None: self.contentFrame.destroy()
        self.contentFrame = Frame(self._frame, bg='#fff')
        self.onupdateFrame()
        self.contentFrame.pack(fill="both", expand=1)

    def actionConsole(self, value):
        if len(self.actions) == value:
            if self.parent is None: return
            else: self.parent.openConsole()
        elif len(self.actions) > value: self.actions[value].function()
        else: raise Exception("Cette action n'existe pas !")

    def openConsole(self):
        pass


class Action:
    def __init__(self, title: str, function: Callable = None, childObject: 'Menu' = None, stateAction="A"):
        """
        :param title: titre vue par l'utilisateur pour reconnaître l'action
        :param function: un callable appellé pour executé au moment voulu (ex: dans un bouton)
        :param childObject (optionel): spécifier un menu enfant pour appliquer le Frame (utile pour les fenêtres)
        :param stateAction: "w" pour une action présente uniquement dans une fenêtre, "c" dans une console et "a" pour tout
        """
        self.title = title
        self.childObject = childObject
        self.function = function
        self.stateAction = stateAction
        if function is None:
            if childObject is not None: self.function = lambda: childObject.open()

    def addFrameToChild(self, frame: 'BaseWidget'):
        self.childObject.setFrame(frame)

    def isConsoleAction(self):
        if self.stateAction.upper() == 'A' or self.stateAction.upper() == 'C': return True
        else: return False

    def isWindowAction(self):
        if self.stateAction.upper() == 'A' or self.stateAction.upper() == 'W': return True
        else: return False

class RestaurantObject:
    def __init__(self, uuid=None):
        self.uuid = generateID() if uuid is None else uuid

    def addToDict(self, dict: Dict[str, 'RestaurantObject']) -> 'RestaurantObject':
        dict[self.uuid] = self
        return self

class ExceptionWrongInformation(Exception):
    """
    Appellé cette exception lorsqu'une information est érronée.
    """
    pass

def mustBeANumber(element, message) -> int:
    try:
        return int(element)
    except ValueError:
        raise ExceptionWrongInformation(message)

def mustBeNotEmptyString(element, message) -> str:
    if element == "" or not isinstance(element, str): raise ExceptionWrongInformation(message)
    return element

def mustBeAFloat(element, message) -> float:
    try:
        return float(element)
    except ValueError:
        raise ExceptionWrongInformation(message)

def generateID():
    """
    Générer une chaine de caractère unique pour les données, ici j'utilise UUID version 4
    :return: une chaine de caractère unique
    """
    return str(uuid.uuid4())


def saveJSON(jsonStr: dict, fileName: str):
    """
    Enregistrer un fichier json
    :param jsonStr: le dictionnaire à enregistrer
    :param fileName: le nom du fichier
    :return:
    """
    chemin = os.getcwd() + "\\saves"
    if not os.path.exists(chemin):
        os.mkdir(chemin)
    os.chdir(chemin)
    if fileName[-4:] != "json": fileName += ".json"
    file = open(fileName, "w", encoding="utf8")
    json.dump(jsonStr, file, sort_keys=True, indent=4, default=json_serial)
    file.close()
    os.chdir('..')


def loadJSON(fileName: str) -> dict:
    """
    Ouvrir un fichier json
    :param fileName: le nom du fichier à ouvrir
    :return: le dictionnaire récuperé dans le fichier
    """
    result = None
    chemin = os.getcwd() + "\\saves"
    if os.path.exists(chemin):
        os.chdir(chemin)
        if fileName[-4:] != "json": fileName += ".json"
        if os.path.exists(fileName):
            with open(fileName) as json_data:
                result = json.load(json_data)
            json_data.close()

    os.chdir('..')
    return result


def json_serial(o):
    if isinstance(o, (datetime.date, datetime.datetime)):
        return o.timestamp()


def contains(list: list, filter):
    for x in list:
        if filter(x):
            return list.index(x)
    return -1

def containsDict(list: dict, filter):
    for x in list:
        if filter(list[x]):
            return x
    return None
