# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#  Auteur: xehx (Edwin HELET)
#  Date: 04/05/2020 15:19
#  Projet: Sondage (Projet Restaurant 1A)
#  Dernière modification: 04/05/2020 14:52
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#  Auteur: xehx (Edwin HELET)
#  Date: 23/04/2020 14:38
#  Projet: Sondage (Projet Restaurant 1A)
#  Dernière modification: 23/04/2020 14:38
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
from tkinter import Frame
from typing import TYPE_CHECKING, Dict, List
import tkinter.messagebox as tm
import math

from menu.gestion_menu.gestion_ingredient import IngredientsMenu
from menu.gestion_stock import GestionStock
from tkcustom.formulaire import FormWindow, EntryProperties, RadioEntryProperties
from tkcustom.tableau import TableauFrame, TableauCallable, TableauColumnProperties
from utils import Menu, Action, loadJSON, saveJSON, ExceptionWrongInformation, RestaurantObject, containsDict, mustBeNotEmptyString, mustBeANumber, mustBeAFloat

if TYPE_CHECKING:
    from main import Restaurant

PlatCategorie: Dict[int, str] = {
    0: "Plat principal",
    1: "Entrée",
    2: "Boisson",
    3: "Dessert",
}


class Plat(RestaurantObject):
    def __init__(self, name: str, categorie: int, price: float, ingredients: List[dict] = [], uuid=None):
        super().__init__(uuid)
        self.setName(name).setCategorie(categorie).setPrice(price)
        self.ingredients = ingredients
        self.disponibility = math.inf

    def setPrice(self, price) -> 'Plat':
        self.price = mustBeAFloat(price, "Le prix doit être un nombre décimal ou entier !")
        return self

    def setName(self, name) -> 'Plat':
        self.name = mustBeNotEmptyString(name, "Le nom doit être une chaîne de caractère nom vide !")
        return self

    def setCategorie(self, categorie) -> 'Plat':
        self.categorie = mustBeANumber(categorie, "La catégorie est un nombre en 0 et 3 !")
        return self

    def addIngredient(self, name, amount):
        if not isinstance(amount, int):
            if not amount.isdigit(): raise ExceptionWrongInformation("La quantité doit être un nombre")
        ingredient = {'name': name, 'amount': int(amount)}
        self.ingredients.append(ingredient)
        return ingredient

    def updateIngredient(self, id, name, amount):
        if not isinstance(amount, int):
            if not amount.isdigit(): raise ExceptionWrongInformation("La quantité doit être un nombre")
        self.ingredients[id] = {'name': name, 'amount': int(amount)}

    def getIndexIngredient(self, ingredient: dict):
        return self.ingredients.index(ingredient)

    def updateDisponibility(self, gestionMenu: 'GestionMenu'):
        if not self.ingredients: self.disponibility = math.inf
        for ingredient in self.ingredients:
            indice = containsDict(gestionMenu.gestionStock.produits, lambda produit: produit.name.lower() == ingredient['name'].lower())
            if indice is None:
                self.disponibility = 0
                return
            else:
                nb = gestionMenu.gestionStock.produits[indice].amount//ingredient['amount']
                if nb < 1:
                    self.disponibility = 0
                    return
                else: self.disponibility = nb if self.disponibility <= 0 or self.disponibility > nb else self.disponibility

    # récupérer l'object dans un dictionnaire, possibilité de choisir pour les données ou pour une vue de l'utilisateur: en particulier pour des catégories par exemple
    def dict(self, toData: bool = True) -> dict:
        ob = self.__dict__.copy()
        if not toData:
            ob['categorie'] = PlatCategorie[self.categorie]
        else:
            del ob['disponibility']
        return ob


class GestionMenu(Menu):
    def __init__(self, parent: 'Restaurant', gestionStock: 'GestionStock'):
        super().__init__([
            Action("Modifier un plat", lambda: None, stateAction="c"),
            Action("Ajouter un plat", lambda: self.openAddPlat()),
            Action("Supprimer un plat", lambda: None, stateAction="c"),
            Action("⭯", lambda: self.updateFrame(), stateAction="w")
        ], parent, onupdateFrame=self.onUpdateFrame)
        self.menu: Dict[str, 'Plat'] = dict()
        self.gestionStock = gestionStock
        self.windowIngredients: Dict[str, IngredientsMenu] = dict()
        self.loadMenu()

    def addPlat(self, name: str, categorie: int, price: float):
        plat = Plat(name, categorie, price)
        plat.addToDict(self.menu)
        self.tableau.addLine(plat.uuid, plat.dict(toData=False))
        if self.parent.currentCommand is not None: self.parent.currentCommand.addPlat(plat)

    def deletePlat(self, plat: 'Plat'):
        if plat.uuid in self.menu:
            self.menu.pop(plat.uuid)
            if self.tableau is not None: self.tableau.removeLine(plat.uuid)
            if self.parent.currentCommand is not None: self.parent.currentCommand.deletePlat(plat)
        else:
            raise Exception(f"L'identifiant '{plat.uuid}' ne correspond à aucun plat.")

    def updatePlat(self, uuid, name: str, categorie: int, price: float):
        if uuid in self.menu:
            plat = self.menu[uuid].setPrice(price).setName(name).setCategorie(categorie)
            self.tableau.updateValue(uuid, price=price, categorie=PlatCategorie[categorie], name=name)
            if self.parent.currentCommand is not None: self.parent.currentCommand.updatePlat(plat)
        else:
            raise Exception(f"L'identifiant '{uuid}' ne correspond à aucun plat.")

    def updateDisponibilities(self):
        """
        Executer dans la gestion des stocks
        :return:
        """
        for uuid in self.menu:
            plat = self.menu[uuid]
            plat.updateDisponibility(self)
            self.tableau.updateValue(plat.uuid, disponibility=plat.disponibility)
            if self.parent.currentCommand is not None: self.parent.currentCommand.updatePlat(plat)

    def getDictList(self, toData: bool = True) -> dict:
        d = dict()
        for k in self.menu: d[k] = self.menu[k].dict(toData)
        return d

    def loadMenu(self):
        json = loadJSON("menu.json")
        if json is not None:
            for k in json:
                element = json[k]
                Plat(element['name'], element['categorie'], element['price'], element['ingredients'], element['uuid']).addToDict(self.menu).updateDisponibility(self)

    def saveMenu(self):
        saveJSON(self.getDictList(), "menu.json")

    # ------------------------------------------------------------------------------------------------------------------
    #                                               PARTIE TKINTER
    # ------------------------------------------------------------------------------------------------------------------
    def openAddPlat(self):
        FormWindow(self.contentFrame,
                   function=self.addPlat,
                   title="Ajouter un plat",
                   name=EntryProperties("Nom du plat"),
                   categorie=RadioEntryProperties("Catégorie", PlatCategorie),
                   price=EntryProperties("Prix (en €)"))

    def openUpdatePlat(self, ob: dict):
        plat = self.menu[ob['uuid']]
        FormWindow(self.contentFrame,
                   function=lambda name, categorie, price: self.updatePlat(plat.uuid, name, categorie, price),
                   title=f"Modifier un plat {plat.uuid}",
                   name=EntryProperties("Nom du plat", plat.name),
                   categorie=RadioEntryProperties("Catégorie", PlatCategorie, plat.categorie),
                   price=EntryProperties("Prix (en €)", plat.price))

    def openDeletePlat(self, ob: dict):
        r = tm.askyesno(title="Supprimer un plat", message="Etes-vous sur de vouloir supprimer cet élement ?")
        if r: self.deletePlat(self.menu[ob['uuid']])

    def openIngredantsMenu(self, ob: dict):

        if not (ob['uuid'] in self.windowIngredients):
            ing = IngredientsMenu(self, self.menu[ob['uuid']])
            self.windowIngredients[ob['uuid']] = ing
        else:
            self.windowIngredients[ob['uuid']].window.deiconify()

    def onUpdateFrame(self):
        Frame(self.contentFrame, bg='#c8c8c8', height=10).pack(fill="x", pady=10)
        self.tableau = TableauFrame(self.contentFrame, self.getDictList(toData=False),
                                    uuid=TableauColumnProperties('Identifiant', str),
                                    name=TableauColumnProperties('Nom', str),
                                    categorie=TableauColumnProperties('Catégorie', str),
                                    disponibility=TableauColumnProperties('Disponibilité', [bool, int]),
                                    price=TableauColumnProperties('Prix (en €)', str),
                                    ingredients=TableauColumnProperties("Nb ingrédients", list),
                                    ingredientBtn=TableauCallable("Ingr", self.openIngredantsMenu),
                                    update=TableauCallable("Edit", self.openUpdatePlat, style="UPD.TButton"),
                                    delete=TableauCallable("Supp", self.openDeletePlat, style="DEL.TButton"),
                                    padx=40)
        self.tableau.pack()
