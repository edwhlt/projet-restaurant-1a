# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#  Auteur: xehx (Edwin HELET)
#  Date: 04/05/2020 15:19
#  Projet: Sondage (Projet Restaurant 1A)
#  Dernière modification: 04/05/2020 15:19
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
from tkinter import Frame
from typing import TYPE_CHECKING

from tkcustom.formulaire import CustomTopLevel, FormWindow, EntryProperties
from tkcustom.tableau import TableauFrame, TableauColumnProperties, TableauCallable
import tkinter.messagebox as tm
from utils import Action, Menu

if TYPE_CHECKING:
    from menu.gestion_menu.gestion_menu import GestionMenu, Plat


class IngredientsMenu(Menu):

    def __init__(self, parent: 'GestionMenu', plat: 'Plat'):
        super().__init__([
            Action("Ajouter un ingrédient", lambda: self.openAddIngredient()),
        ], parent, onupdateFrame=self.onUpdateFrame)
        self.plat = plat

        self.window = CustomTopLevel(self.parent.contentFrame, f"Gestion des ingrédients de {plat.uuid}", resizable=False, bg='#fff')
        self.window.geometry("400x600")
        self.window.maxsize("1280", "720")
        self.window.protocol("WM_DELETE_WINDOW", self.close)
        self.setFrame(self.window)

    def close(self):
        self.plat.updateDisponibility(self.parent)
        if self.parent.tableau is not None: self.parent.tableau.updateValue(self.plat.uuid, disponibility=self.plat.disponibility, ingredients=self.plat.ingredients)
        if self.parent.parent.currentCommand is not None: self.parent.parent.currentCommand.updatePlat(self.plat)
        del self.parent.windowIngredients[self.plat.uuid]
        self.window.destroy()

    def addIngredient(self, name, amount):
        ingredient = self.plat.addIngredient(name, amount)
        if self.tableau is not None: self.tableau.addLine(self.plat.ingredients.index(ingredient), ingredient)

    def deleteIngredient(self, id):
        if id < len(self.plat.ingredients):
            self.plat.ingredients.pop(id)
            if self.tableau is not None: self.tableau.removeLine(id)
        else:
            raise Exception(f"L'ingrédient n°{id} ne correspond à aucun ingrédient dans ce plat.")

    def updateIngredient(self, id, name: str, amount: int):
        if id < len(self.plat.ingredients):
            self.plat.updateIngredient(id, name, amount)
            if self.tableau is not None: self.tableau.updateValue(id, name=name, amount=amount)
        else:
            raise Exception(f"L'ingrédient n°{id} ne correspond à aucun ingrédient dans ce plat.")

    def getIngredientDictList(self) -> dict:
        d = dict()
        for k in range(len(self.plat.ingredients)):
            d[k] = self.plat.ingredients[k]
        return d

    # ------------------------------------------------------------------------------------------------------------------
    #                                               PARTIE TKINTER
    # ------------------------------------------------------------------------------------------------------------------

    def openAddIngredient(self):
        FormWindow(self.contentFrame, function=self.addIngredient,
                   title="Ajouter un produit",
                   name=EntryProperties("Nom du produit"),
                   amount=EntryProperties("Quantité"),
                   message="Attention pour le calcule de la disponibilité vérifiez bien\nque le nom du produit que vous allez entrer correspond à un produit dans la gestion des produits !")

    def openUpdateIngredient(self, ob: dict):
        id = self.plat.getIndexIngredient(ob)
        FormWindow(self.contentFrame,
                   function=lambda name, amount: self.updateIngredient(id, name, amount),
                   title=f"Modifier l'ingrédient n°{id}",
                   name=EntryProperties("Nom du plat", ob['name']),
                   amount=EntryProperties("Quantité", ob['amount']))

    def openDelectIngredient(self, ob: dict):
        r = tm.askyesno(parent=self.window, title="Supprimer un ingrédient", message="Etes-vous sur de vouloir supprimer cet élement ?")
        if r: self.deleteIngredient(self.plat.getIndexIngredient(ob))

    def onUpdateFrame(self):
        Frame(self.contentFrame, bg='#c8c8c8', height=10).pack(fill="x", pady=10)
        self.tableau = TableauFrame(self.contentFrame, self.getIngredientDictList(),
                                    name=TableauColumnProperties('Nom', str),
                                    amount=TableauColumnProperties('Quantité', str),
                                    update=TableauCallable("Upd", self.openUpdateIngredient, style="UPD.TButton"),
                                    delete=TableauCallable("Del", self.openDelectIngredient, style="DEL.TButton"),
                                    padx=60)
        self.tableau.pack()
