# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#  Auteur: xehx (Edwin HELET)
#  Date: 26/04/2020 21:29
#  Projet: Sondage (Projet Restaurant 1A)
#  Dernière modification: 26/04/2020 21:29
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
from tkinter import Label, filedialog, Frame
from typing import TYPE_CHECKING, List
import tkinter.messagebox as tm
import datetime

from utils import Menu, Action

if TYPE_CHECKING:
    from menu.gestion_commandes import GestionCommandes, Commande
    from main import Restaurant

class EntryLine:

    def __init__(self, title, value, inverted=False):
        self.title = title
        self.value = value
        self.inverted = inverted

    def generateLine(self, master, row):
        left = Label(master, text=f"{self.title} : ", bg="#fff", font=('calibri', 12))
        right = Label(master, text=self.value, bg="#fff", font=('calibri', 12, 'bold'))
        if self.inverted:
            left.config(text=f"{self.value} : ", font=('calibri', 12, 'bold'))
            right.config(text=self.title, font=('calibri', 12))
        left.grid(row=row, sticky='e', pady=5)
        right.grid(row=row, column=1, sticky='w', pady=5)


class CommandsHistory(Menu):

    def __init__(self, parent: 'Restaurant', gestionCommands: 'GestionCommandes'):
        super().__init__([
            Action("Exporter le résumé", lambda: self.exportHistory()),
            Action("⭯", lambda: self.updateFrame(), stateAction="w")
        ], parent, onupdateFrame=self.onUpdateFrame)
        self.gestionCommands = gestionCommands
        self.frameDatas = None

        self.nbCommands = len(self.gestionCommands.clients)
        self.priceSumTot = sum(self.gestionCommands.clients[uuid].getPrice() for uuid in self.gestionCommands.clients)

        dateNow = datetime.datetime.now()
        lastWeek = dateNow.timestamp() - (7 * 24 * 3600)

        commandsWeek = [self.gestionCommands.clients[uuid].getPrice() for uuid in self.gestionCommands.clients if self.gestionCommands.clients[uuid].date.timestamp() >= lastWeek]
        self.nbCommandsWeek = len(commandsWeek)
        self.priceSumWeek = sum(commandsWeek)

    def strsResume(self) -> List[EntryLine]:
        strs = [
            EntryLine("Nombre total de commandes passées", self.nbCommands),
            EntryLine("Commandes passées ces 7 derniers jours", self.nbCommandsWeek),
            EntryLine("Montant total des commandes ces 7 derniers jours (en €)", self.priceSumWeek),
            EntryLine("Montant moyen d'une commande (en €)", 0 if self.nbCommands == 0 else self.priceSumTot / self.nbCommands)
        ]
        return strs

    def exportHistory(self):
        file = filedialog.asksaveasfile(mode='w', defaultextension=".txt", filetypes=[("Text Files", ".txt")], initialfile="resume.txt")
        if file is not None:
            strs = self.strsResume()
            file.write("\n".join(f"{element.title}: {element.value}" for element in strs))
            file.close()
            tm.showinfo("Information", "Historique des commandes exporté avec succès")

    def updateAndAddCommande(self, commande: 'Commande'):
        self.nbCommands += 1
        self.priceSumTot += commande.getPrice()
        self.nbCommandsWeek += 1
        self.priceSumWeek += commande.getPrice()
        self.updateFrame()

    def onUpdateFrame(self):
        strs = self.strsResume()
        Frame(self.contentFrame, bg='#c8c8c8', height=10).pack(fill="x", pady=10)
        self.frameDatas = Frame(self.contentFrame, height=580, bg='#fff')

        for i in range(len(strs)):
            strs[i].generateLine(self.frameDatas, i)
        self.frameDatas.grid_columnconfigure(0, minsize=500)
        self.frameDatas.grid_columnconfigure(1, minsize=500)
        self.frameDatas.pack()
