# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#  Auteur: xehx (Edwin HELET)
#  Date: 22/04/2020 21:49
#  Projet: Sondage (Projet Restaurant 1A)
#  Dernière modification: 22/04/2020 21:49
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from tkinter import filedialog, Frame
import tkinter.messagebox as tm
from typing import TYPE_CHECKING, Dict

from tkcustom.formulaire import FormWindow, EntryProperties, RadioEntryProperties
from tkcustom.tableau import TableauCallable, TableauColumnProperties, TableauFrame
from utils import Menu, Action, loadJSON, saveJSON, RestaurantObject, mustBeNotEmptyString, mustBeANumber

if TYPE_CHECKING:
    from main import Restaurant

Unit: Dict[int, str] = {
    0: "Sans unité",
    1: "Gramme",
    2: "Mililitre",
    3: "Kilogramme",
    4: "Litre",
}

class Produit(RestaurantObject):
    def __init__(self, name, amount, unit, uuid: str = None):
        super().__init__(uuid)
        self.setName(name).setAmount(amount).setUnit(unit)

    def setName(self, name) -> 'Produit':
        self.name = mustBeNotEmptyString(name, message="Le nom doit être une chaîne de caractère non vide !")
        return self

    def setAmount(self, amount) -> 'Produit':
        self.amount = mustBeANumber(amount, message="La quantité doit être un nombre !")
        return self

    def setUnit(self, unit) -> 'Produit':
        self.unit = mustBeANumber(unit, "L'unité est un nombre en 0 et 4 !")
        return self

    # récupérer l'object dans un dictionnaire, possibilité de choisir pour les données ou pour une vue de l'utilisateur: en particulier pour des catégories par exemple
    def dict(self, toData: bool = True) -> dict:
        ob = self.__dict__.copy()
        if not toData: ob['unit'] = Unit[self.unit]
        return ob

class GestionStock(Menu):
    def __init__(self, parent: 'Restaurant'):
        super().__init__([
            Action("Ajout de produits", lambda: self.openAddProduct()),
            Action("Exporter la liste des produits", lambda: self.exportProducts()),
            Action("Supprimer un produit", lambda: None, stateAction="c"),
            # Action("Activer/désactiver les titres des colonnes", lambda: self.tableau.updateViewColumn(), stateAction="w"),
            Action("⭯", lambda: self.updateFrame(), stateAction="w")
        ], parent, onupdateFrame=self.onUpdateFrame)
        self.produits: Dict[str, 'Produit'] = dict()
        self.loadProduit()

    def addProduit(self, name, amount, unit):
        produit = Produit(name, amount, unit).addToDict(self.produits)
        if self.tableau is not None: self.tableau.addLine(produit.uuid, produit.dict(toData=False))
        # on update la disponibilité des plats puisque on modifie les produits en stock
        self.parent.menu.updateDisponibilities()

    def deleteProduct(self, uuid):
        if uuid in self.produits:
            self.produits.pop(uuid)
            if self.tableau is not None: self.tableau.removeLine(uuid)
            # on update la disponibilité des plats puisque on modifie les produits en stock
            self.parent.menu.updateDisponibilities()
        else:
            raise Exception(f"L'identifiant '{uuid}' ne correspond à aucun produit.")

    def updateProduct(self, uuid, name, amount, unit):
        if uuid in self.produits:
            self.produits[uuid].setAmount(amount).setName(name).setUnit(unit)
            if self.tableau is not None:
                self.tableau.updateValue(uuid, amount=amount, name=name, unit=Unit[unit])
            # on update la disponibilité des plats puisque on modifie les produits en stock
            self.parent.menu.updateDisponibilities()
        else:
            raise Exception(f"L'identifiant '{uuid}' ne correspond à aucun produit.")

    def getDictList(self, toData: bool = True) -> dict:
        d = dict()
        for k in self.produits: d[k] = self.produits[k].dict(toData)
        return d

    def loadProduit(self):
        json = loadJSON("produits.json")
        if json is not None:
            for k in json:
                element = json[k]
                Produit(element['name'], element['amount'], element['unit'], element['uuid']).addToDict(self.produits)

    def saveProducts(self):
        saveJSON(self.getDictList(), "produits.json")

    # ------------------------------------------------------------------------------------------------------------------
    #                                               PARTIE TKINTER
    # ------------------------------------------------------------------------------------------------------------------

    def exportProducts(self):
        file = filedialog.asksaveasfile(mode='w', defaultextension=".txt", filetypes=[("Text Files", ".txt")], initialfile="produits.txt")
        if file is not None:
            for uuid in self.produits: file.write("Nom: {:s}, Quantité: {:d} {:s}\n".format(self.produits[uuid].name, self.produits[uuid].amount, Unit[self.produits[uuid].unit]))
            file.close()
            tm.showinfo("Information", "Fichier des produits exporté !")

    def openAddProduct(self):
        FormWindow(self.contentFrame,
                   function=self.addProduit,
                   title="Ajouter un produit",
                   name=EntryProperties("Nom du produit"),
                   unit=RadioEntryProperties("Unité", Unit),
                   amount=EntryProperties("Quantité"))

    def openUpdateProduct(self, ob: dict):
        product = self.produits[ob['uuid']]
        FormWindow(self.contentFrame,
                   function=lambda name, unit, amount: self.updateProduct(product.uuid, name, amount, unit),
                   title=f"Modifier le produit {product.uuid}",
                   name=EntryProperties("Nom du produit", product.name, state='disabled'),
                   unit=RadioEntryProperties("Unité", Unit, product.unit),
                   amount=EntryProperties("Quantité", product.amount))

    def openDeleteProduct(self, ob: dict):
        r = tm.askyesno(title="Supprimer un produit", message="Etes-vous sur de vouloir supprimer cet élement ?")
        if r: self.deleteProduct(ob['uuid'])

    def onUpdateFrame(self):
        Frame(self.contentFrame, bg='#c8c8c8', height=10).pack(fill="x", pady=10)
        self.tableau = TableauFrame(self.contentFrame, self.getDictList(toData=False),
                                    uuid=TableauColumnProperties('Identifiant', str),
                                    name=TableauColumnProperties('Nom', str),
                                    amount=TableauColumnProperties('Quantité', str),
                                    unit=TableauColumnProperties("Unité", str),
                                    update=TableauCallable("Edit", self.openUpdateProduct, style="UPD.TButton"),
                                    delete=TableauCallable("Supp", self.openDeleteProduct, style="DEL.TButton"), padx=50)
        self.tableau.pack()
