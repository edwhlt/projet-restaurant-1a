# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#  Auteur: xehx (Edwin HELET)
#  Date: 23/04/2020 20:18
#  Projet: Sondage (Projet Restaurant 1A)
#  Dernière modification: 23/04/2020 20:18
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import datetime
import os
from tkinter.ttk import *
from tkinter import Frame, Label
from typing import TYPE_CHECKING, Dict
import tkinter.messagebox as tm

from menu.gestion_menu.gestion_menu import Plat, PlatCategorie, GestionMenu
from tkcustom.formulaire import FormWindow, EntryProperties, CustomTopLevel
from tkcustom.tableau import TableauFrame, TableauColumnProperties, TableauCallable
from utils import Menu, Action, loadJSON, saveJSON, ExceptionWrongInformation, contains, RestaurantObject

if TYPE_CHECKING:
    from main import Restaurant


class Commande(RestaurantObject):
    def __init__(self, clientName: str = "", plats: dict = {}, date=None, uuid=None):
        super().__init__(uuid)
        self.date = datetime.datetime.now() if date is None else date
        self.clientName = clientName
        self.plats = plats

    def addPlat(self, plat: 'Plat', amount: int):
        self.plats[plat.uuid] = {'name': plat.name, 'price': plat.price, 'amount': amount}

    def removePlat(self, platUuid: str):
        if platUuid in self.plats: self.plats.pop(platUuid)

    def getPrice(self):
        return sum(self.plats[uuid]['price'] * self.plats[uuid]['amount'] for uuid in self.plats)


class GestionCommandes:
    def __init__(self, gestionMenu: 'GestionMenu'):
        self.clients: Dict[str, 'Commande'] = dict()
        self.gestionMenu = gestionMenu
        self.loadCommandes()

    def addCommande(self, nameClient: str, commandes: dict):
        commande = Commande(nameClient, commandes)
        commande.addToDict(self.clients)
        return commande

    def loadCommandes(self):
        json = loadJSON("commandes.json")
        if json is not None:
            for k in json:
                element = json[k]
                Commande(element['clientName'], element['plats'], datetime.datetime.fromtimestamp(element['date']), element['uuid']).addToDict(self.clients)

    def saveCommandes(self):
        json = dict()
        for k in self.clients:
            json[k] = self.clients[k].__dict__
        saveJSON(json, "commandes.json")


class PriseCommande(Menu):
    def __init__(self, parent: 'Restaurant', clientName, gestionMenu: 'GestionMenu', gestionCommandes: 'GestionCommandes'):
        super().__init__([
            Action("Selectionner un produit", lambda: self.openSelectPlatMain()),
            Action("Resumé de la commande", lambda: self.openResume()),
            Action("Envoie de la commande en cuisine", lambda: self.sendToKitchen()),
            Action("⭯", lambda: self.updateFrame(), stateAction="w")
        ], parent, onupdateFrame=self.onUpdateFrame)
        self.gestionMenu = gestionMenu
        self.gestionCommandes = gestionCommandes

        self.frameOnglet = Frame(self.parent.tab, bg="#fff", borderwidth=0)
        self.menuFrame = None
        self.tableaux = dict()
        self.menuAffichage = list()

        self.commande = Commande(clientName=clientName)

        self.setFrame(self.frameOnglet)
        # self.menuAffichage.sort(key=lambda x: x.categorie)

        self.frameOnglet.pack_propagate(0)
        self.frameOnglet.pack(fill='both', expand=1)
        self.parent.tab.add(self.frameOnglet, text=f"Prise de commande du client: {clientName}")

        footer = self.parent.footer
        self.leaveButton = Button(footer, text="Annuler la commande en cours",
                                  command=lambda:
                                  FormWindow(self.contentFrame,
                                             function=lambda: self.leave(), title="Annuler la commande",
                                             message="Etes-vous sur de vouloir quitter la commande en cours ?\nSi vous ne l'avez pas envoyé en cuisine la commande ne sera pas enregistré !"),
                                  style="DEL.TButton")
        self.leaveButton.grid(row=0, column=1, padx=20, pady=5, sticky='e')
        self.parent.tab.select(3)

    def addPlatInCommand(self, index: int, amount: int):
        if len(self.menuAffichage) > index:
            plat = self.menuAffichage[index]
            if plat.disponibility:
                lastAmount = self.commande.plats[self.menuAffichage[index].uuid]['amount'] if self.menuAffichage[index].uuid in self.commande.plats else 0
                if plat.disponibility >= amount+lastAmount:
                    self.commande.addPlat(self.menuAffichage[index], amount)
                else: raise ExceptionWrongInformation(f"Ce plat est disponible mais il n'en reste que {plat.disponibility} hors vous en voulez {amount+lastAmount} (dont {lastAmount} selectionné "
                                                      f"avant). Modifier la quantité ou choisissez un autre plat !")
            else:
                raise ExceptionWrongInformation("Ce plat est indisponible pour le moment !")
        else:
            raise ExceptionWrongInformation("Le numéro du plat n'existe pas.")

    def addPlat(self, plat: 'Plat'):
        self.menuAffichage.append(plat)
        ob = plat.dict(toData=False)
        ob['id'] = self.menuAffichage.index(plat)
        self.tableaux[plat.categorie].addLine(plat.uuid, ob)

    def updatePlat(self, plat: 'Plat'):
        index = contains(self.menuAffichage, lambda e: e.uuid == plat.uuid)
        if index == -1: return

        self.menuAffichage.insert(index, plat)
        for tabID in self.tableaux:
            tab = self.tableaux[tabID]
            if tabID == plat.categorie:
                tab.updateValue(plat.uuid, name=plat.name, price=plat.price, disponibility=plat.disponibility)
            else: tab.removeLine(plat.uuid)

    def deletePlat(self, plat: 'Plat'):
        self.menuAffichage.remove(plat)
        self.tableaux[plat.categorie].removeLine(plat.uuid)

    def resumeText(self) -> list:
        strs = [f"Client de la commande: {self.commande.clientName}", f"Date et heure: {self.commande.date.strftime('%m/%d/%Y, %H:%M:%S')}",
                f"Prix total de la commande: {self.commande.getPrice()}€", " ", "Liste des plats choisies: "]
        if not self.commande.plats: strs.append("Aucun plats choisie")
        else:
            for uuid in self.commande.plats:
                plat = self.commande.plats[uuid]
                strs.append(f"  - Plat: {plat['name']} - Quantité:  {plat['amount']} - Prix: {plat['price'] * plat['amount']}€")
        return strs

    def removePlat(self, plat: 'Plat'):
        self.commande.removePlat(plat.uuid)
        self.tableaux[plat.categorie].removeLine(plat.uuid)

    # ------------------------------------------------------------------------------------------------------------------
    #                                               PARTIE TKINTER
    # ------------------------------------------------------------------------------------------------------------------

    def openSelectPlatMain(self):
        FormWindow(self.contentFrame,
                   function=lambda id, amount: self.addPlatInCommand(int(id), int(amount)),
                   title="Ajouter un plat",
                   id=EntryProperties("Numéro du plat"),
                   amount=EntryProperties("Quantité"))

    def openSelectPlat(self, ob: dict):
        FormWindow(self.contentFrame,
                   function=lambda amount: self.addPlatInCommand(int(ob['id']), int(amount)),
                   title="Ajouter un plat",
                   amount=EntryProperties("Quantité"))

    def getMenuDict(self, categorie) -> dict:
        menu = self.menuAffichage
        ob = dict()
        for i in range(len(menu)):
            plat = menu[i]
            if plat.categorie == categorie:
                ob[plat.uuid] = plat.dict(toData=False)
                ob[plat.uuid]['id'] = i
        return ob

    def openResume(self):
        window = CustomTopLevel(self.contentFrame, title="Résumé de la commande", resizable=False, bg='#fff')
        frame = Frame(window, bg='#fff')
        text = Label(frame, text="\n".join(self.resumeText()), font=('calibri', 11), justify="left", bg='#fff')
        text.pack(side="left", padx=20, pady=20)
        frame.pack()

    def sendToKitchen(self):
        chemin = os.getcwd() + "\\en_cuisine"
        if not os.path.exists(chemin):
            os.mkdir(chemin)
        os.chdir(chemin)
        file = open(f"resume_commande_{self.commande.uuid}.txt", "w", encoding="utf8")
        file.write("\n".join(str for str in self.resumeText()))
        file.close()
        os.chdir('..')
        self.commande.addToDict(self.gestionCommandes.clients)
        self.parent.history_commands.updateAndAddCommande(self.commande)
        tm.showinfo("Information", "Commande envoyée en cuisine et sauvegardée !")

    def onUpdateFrame(self):
        self.menuAffichage = [self.gestionMenu.menu[uuid] for uuid in self.gestionMenu.menu]
        self.tableaux.clear()
        Frame(self.contentFrame, bg='#c8c8c8', height=10).pack(fill="x", pady=10)
        contentTab = Frame(self.contentFrame, bg="#fff")

        for cat in PlatCategorie:
            platsByCategorie = self.getMenuDict(cat)

            if platsByCategorie:
                label = Label(contentTab, text=PlatCategorie[cat], font=('calibri', 13, 'underline', 'bold'), bg='#fff')
                label.grid(row=0, column=cat)
                tableau = TableauFrame(contentTab, platsByCategorie,
                                       id=TableauColumnProperties('Identifiant', str),
                                       name=TableauColumnProperties('Nom', str),
                                       price=TableauColumnProperties('Prix (en €)', str),
                                       disponibility=TableauColumnProperties('Disponibilité', [bool, int]),
                                       add=TableauCallable("+", self.openSelectPlat, style="INFO.TButton"),
                                       columnView=False, padx=10, bd=1)
                tableau.grid(row=1, column=cat, sticky="ns")
                contentTab.grid_columnconfigure(cat, weight=1)
                self.tableaux[cat] = tableau
        contentTab.grid_propagate(0)
        contentTab.pack(fill="both", expand=1)

    def leave(self):
        self.parent.currentCommand = None
        n = self.parent.tab.index(self.parent.tab.select())
        if n == 3: self.parent.tab.select(0)
        self.leaveButton.destroy()
        self._frame.destroy()
